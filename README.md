# Opssmart EPCIS Simple Document : Interoperability B2B Workflow (5 Step)

## Step 1 : Buyer request EPCIS Query Interface URL using linkType=gs1:epcis

### HTTP Verb [GET]
### GS1 Digital Link URL <br>
`{Base URL}/{Application Identifier}/{Value 1}/{Application Identifier 2}/{Value 2}`

gtin (01) / lot (10) <br>
Example : https://resolver.example.org/gtin/00860003130308/lot/LOT07082021?linkType=gs1:epcis

gtin (01) / ser (21) <br>
Example : https://resolver.example.org/gtin/00860003130308/ser/ABCD-EFGH-1234-5678?linkType=gs1:epcis

### Request Headers

| Name          | Value       | Required  |
| -----------   | ----------- | --------- |
| X-API-Key     | {API KEY}         |  :x:                |
| Content-Type  | application/json  |  :white_check_mark: |
| Accept        | application/xml   |  :white_check_mark: |

### Request Body `No body provided`

### Response Headers

| Name          | Value       |
| -----------   | ----------- |
| Content-Type  | application/json  |

### Response Body
```
    [{
        "link": "https://example.org/EPCIS",
        "title": "EPCIS Query Interface",
        "linkType": "gs1:epcis",
        “authRequired”: “true”
    }]
```


## Step 2 : Buyer request all pertaining events

### HTTP Verb [POST]
URL : `{Base URL}/queries/SimpleEventQuery`

### Request Headers

| Name          | Value       |
| -----------   | ----------- |
| X-API-Key             | {API KEY}         |
| Content-Type          | application/json  |
| Accept                | application/xml   |
| GS1-EPCIS-Version     | 1.2               |
| GS1-EPCIS-Min         | 1.2               |
| GS1-EPCIS-Max         | 1.2               |
| GS1-CBV-Version       | 1.2               |
| GS1-EPC-Version       | ALWAYS_EPC_URN    |
| GS1-CBV-XML-Format    | ALWAYS_URN        |

### Request Body
```
{
    "query": {
        “GE_recordTime” : “2021-11-14T00:00:00Z”,
        “LE_recordTime” : “2021-11-15T00:00:00Z”
        “eventTypes” : [“ObjectEvent”],
        “EQ_bizStep” : “urn:epcglobal:cbv:bizstep:receiving”
    },
    "queryType" : "events" 
}
```

### Response Headers

| Name          | Value       |
| -----------   | ----------- |
| Content-Type          | application/xml   |
| GS1-EPCIS-Version     | 1.2               |
| GS1-EPCIS-Min         | 1.2               |
| GS1-EPCIS-Max         | 1.2               |
| GS1-CBV-Version       | 1.2               |
| GS1-EPC-Format        | Always_GS1_Digital_Link       |
| GS1-CBV-XML-Format    | Always_Web_URI                |

### Response Body

```
    <document>XML</document>
```

## Step 3 : Buyer scan the received EPCIS events to find unknown master data

*It is possible for the master data to be embedded in the header of the EPCIS file as well. (from step 2)

## Step 4 : Buyer request unknown GTINs, GLNs, PGLNs using linkType=gs1:masterData

### HTTP Verb [GET]
### GS1 Digital Link URL <br>

party (417) <br>
Example : https://resolver.example.org/party/0860003130308?linkType=gs1:masterData

gln (416) <br>
Example : https://resolver.example.org/gln/urn:gdst:example.org:location:loc:FARM1.0?linkType=gs1:masterData

gtin (01) <br>
Example : https://resolver.example.org/gtin/00860003130308?linkType=gs1:masterData

### Request Headers

| Name          | Value       |
| -----------   | ----------- |
| X-API-Key     | {API KEY}         |
| Content-Type  | application/json  |
| Accept        | application/json  |

### Request Body `No body provided`

### Response Headers

| Name          | Value       |
| -----------   | ----------- |
| Content-Type  | application/json  |

### Response Body
```
    [{
        "link": "https://example.org/api/products/ABC/123",
        "title": "Product Definition",
        "linkType": "gs1:masterData",
        "mimeType": "application/json"
    }]
```

## Step 5 : Buyer request master data

### HTTP Verb [POST]
URL : `{Base URL}/queries/SimpleEventQuery`

### Request Headers

| Name          | Value       |
| -----------   | ----------- |
| X-API-Key             | {API KEY}         |
| Content-Type          | application/json  |
| Accept                | application/xml   |
| GS1-EPCIS-Version     | 1.2               |
| GS1-EPCIS-Min         | 1.2               |
| GS1-EPCIS-Max         | 1.2               |
| GS1-CBV-Version       | 1.2               |
| GS1-EPC-Version       | ALWAYS_EPC_URN    |
| GS1-CBV-XML-Format    | ALWAYS_URN        |

### Request Body
```
{
    "query": {
        “MATCH_epc” : [
            “<insert_epc_1>”,
            “<insert_epc_2>”,
            …
            “<insert_epc_n>”
        ]
    },
    "queryType" : "events" 
}
```

### Response Headers

| Name          | Value       |
| -----------   | ----------- |
| Content-Type          | application/xml   |
| GS1-EPCIS-Version     | 1.2               |
| GS1-EPCIS-Min         | 1.2               |
| GS1-EPCIS-Max         | 1.2               |
| GS1-CBV-Version       | 1.2               |
| GS1-EPC-Format        | Always_GS1_Digital_Link       |
| GS1-CBV-XML-Format    | Always_Web_URI                |

### Response Body
```
    <document>XML2</document>
```
